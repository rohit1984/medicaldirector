using MedicalDirector.Core;
using NUnit.Framework;

namespace MedicalDirector.Test
{
    public class PatientGroupTest
    {

        [Test]
        public void CalculatePatientGroupTest()
        {
            int[,] matrix = new int[,] {
                { 1, 1, 0, 0, 0, 0 },
                { 0, 1, 0, 0, 0, 0 },
                { 1, 0, 1, 0, 0, 0 },
                { 0, 0, 0, 0, 1, 0},
                { 0, 0, 0, 0, 0, 1},
                { 1, 1, 0, 1, 0, 0}
                                      };

            CalculatePatientGroup calculatePatientGroup = new CalculatePatientGroup();
            var patientGroup = calculatePatientGroup.CalcPatientGroup(matrix);

            Assert.NotZero(patientGroup);
        }
    }
}