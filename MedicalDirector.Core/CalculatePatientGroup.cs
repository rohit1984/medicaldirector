﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MedicalDirector.Core
{
    public class CalculatePatientGroup
    {
        public int CalcPatientGroup(int[,] matrix)
        {

            List<int> adjacentCols = new List<int>();
            List<int> verticalRows = new List<int>();

            int DiagonalValue = 0;
            int diagonalCnt = 0;
            int verticalCnt = 0;
            int horizonatalCnt = 0;

            for (var row = 0; row < matrix.GetLength(0); row++)
            {
                for (var col = 0; col < matrix.GetLength(1); col++)
                {
                    if (row != matrix.GetLength(0) - 1)
                    {
                        if (matrix[row, col] == 1 && matrix[row + 1, col] == 1)
                        {
                            verticalRows.Add(row);
                            verticalRows.Add(row + 1);

                            verticalCnt += 1;
                        }
                    }
                    if (col != matrix.GetLength(1) - 1)
                    {
                        if (matrix[row, col] == 1 && matrix[row, col + 1] == 1)
                        {
                            adjacentCols.Add(row);
                            horizonatalCnt += 1;
                        }
                    }
                }
            }

            bool IsDiagonal = false;

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (i == j)
                    {
                        if (i == 0 && matrix[i, j] == 1)
                        {
                            DiagonalValue = matrix[i, j];
                        }

                        if (!IsDiagonal)
                        {
                            if (i == 1 && matrix[i, j] == DiagonalValue)
                            {
                                diagonalCnt = diagonalCnt + 1;
                                IsDiagonal = true;
                            }
                        }
                    }
                }
            }

            int patientGroup = verticalCnt + horizonatalCnt + diagonalCnt;

            return patientGroup;
        }
    }
}
