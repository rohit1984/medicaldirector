﻿using MedicalDirector.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalDirector.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientGroupsController : ControllerBase
    {
        public PatientGroupsController()
        {

        }

        [HttpPost("calculate")]
        public IActionResult Calculate([FromQuery] int[,] matrix)
        {
            CalculatePatientGroup calculatePatientGroup = new CalculatePatientGroup();
            
            var patientGroup = calculatePatientGroup.CalcPatientGroup(matrix);
            
            return Ok(patientGroup); 
        }
    }
}
